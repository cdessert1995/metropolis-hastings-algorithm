import sys
import numpy as np
from numpy.random import normal
from numpy.random import uniform
from numpy import linspace
from scipy.stats import norm
import matplotlib.pyplot as plt
import time

#The Metropolis-Hastings algorithm
def Metropolis_Hastings(array, std):
	#Avoid dots so the function references are not reevaluated each time through the loop
	norm_pdf = norm.pdf
	
	#Generates the values
	def Generate_value(value, std):
		proposal = normal(value, std, None) #Draw a new proposal state 
		
		#Calculate acceptance probability
		target_ratio = norm_pdf(proposal)/norm_pdf(value) 
		proposal_ratio = norm_pdf(value - proposal)/norm_pdf(proposal - value)
		parameter = target_ratio * proposal_ratio #If parameter < 1, it is the acceptance probability
		
		#Decide whether or not to accept the proposed value
		if parameter >= 1: #Accept the proposed value and add to num_accepted
			next_value = proposal
			accepted = True
		else: #Accept with probability 'parameter'
			draw = uniform(0.0, 1.0, None) #Draw from uniform distribution
			if draw < parameter: #Accept the proposed value with probability 'parameter' and add to num_accepted
				next_value = proposal
				accepted = True
			else: #Discard the proposed value with probability 1 - 'parameter'
				next_value = value
				accepted = False
		return accepted, next_value

	num_accepted = 0 #Counts the number of accepted values throughout the algorithm (for determining acceptance rate)

	#Perform one interation many times, recording the values and keeping track of number accepted
	for i in range(len(array)-1):
		accepted, next_value = Generate_value(array[i], std) #Generate the values
		if accepted == True:
			num_accepted += 1
		array[i+1] = next_value #Record the value chosen in array so we can do an iteration with it
		
	return num_accepted #Return the total number of accepted proposal states

#Read in the target distribution
#Give several options, now just Gaussian

#Read in the proposal density
#Give several options, now just Gaussian

#Declare necessary variables
burn_in_length = 1000 #Number of iterations discarded at the beginning
initial = 0.0
burn_in_states = np.empty(burn_in_length, dtype = float) #burn_in_states records the values for burn-in
burn_in_states[0] = initial #Use initial as the first value for burn-in
std = 2.0 #Changing this value would tune the acceptance rate with an inverse relationship. I found that 2.0 corresponds to a 50% acceptance rate for 1D normals, so I've fixed std for now

repeats = int(float(sys.argv[1]))
if repeats <= 0:
	print 'The number of iterations must be a positive integer. Exiting...'
	sys.exit()

#Burn-in period
Metropolis_Hastings(burn_in_states, std) #Only run Metropolis-Hastings, not interested in number of accepted states
starting_value = burn_in_states[burn_in_length - 1] #Take last value generated
states = np.empty(repeats, dtype = float) #states records the values for the algorithm
states[0] = starting_value #Use this initial value. Thus 'initial' is forgotten.

#Perform the algorithm
num_accepted = Metropolis_Hastings(states, std) #Run Metropolis-Hastings and get the number of accepted states

#Final calculations
acceptance_rate = num_accepted/float(len(states)) #Ideally 50% for 1D Gaussian, down to 23% for higher dimensions
mu, std = norm.fit(states)
xmin, xmax = plt.xlim()
x = np.linspace(-4, 4, 100) #This should work for (xmin, xmax, 100) but only graphs from 0 to 1...
p = norm.pdf(x, mu, std)
plt.plot(x, p, 'k', linewidth = 2)
title = "Fit Results: mean = %.2f, std = %.2f, acceptance = %.2f." % (mu, std, acceptance_rate)
plt.title(title)
plt.hist(states, bins=100, normed = True, alpha = 0.6, color = 'g')
plt.show() #Show histogram and best fit line with some results
