import pstats

p = pstats.Stats('profile_output.txt')

#Ask user how they want the output formatted
option = input('Enter 0 for cumulative stats, 1 for total time stats, or 2 to first sort by total time and then by cumulative time.\n')
number = input('Enter number of lines desired.\n')

#Sort and output
if option == 0:
	p.sort_stats('cumulative').print_stats(number)
elif option == 1:
	p.sort_stats('time').print_stats(number)
else:
	p.sort_stats('time', 'cumulative').print_stats(number)
